# [Home task related to QA Automation Engineer position in Strypes](/Automation%20QA_%20Interview%20Task.pdf)



## Testing of the page's header


## Scope
- All menus and menu items
- Logo link
- Search
  
TestCases: [TestCases.md](/TestCases.md)

## Technology
- [Playwright/TypeScript](https://playwright.dev/)
- [Cucumber](https://cucumber.io/)
- [Playwright-bdd](https://vitalets.github.io/playwright-bdd/#/)

- We are using Cucumber/Gherkin Syntax to define tests, Playwright to implement and execute them and Playwright-bdd to convert BDD tests to Playwright-tests.

## Setup
- Clone the project from GitLab
```
https://gitlab.com/St-Nikolov/strypes-task.git
```
- Install needed modules
```
npm install
```
- Install Playwright
```
npx playwright install
```

## Run tests
- Run all tests
```
npm run test
```
- Run only test with specified tag(s)
```
npm run test-by-tag --tag=@ST001
```
- Start fancy UI runner where you can run and inspect the tests
```
npm run test-ui
```    

## Configuration
- Use playwright.config.ts to choose Browser(s),ViewPort,Parallelization,TimeOuts,Output artifacts and so on. 

## Reports
- Html report is generated in ./playwright-report folder and is automaticaly opened after execution with failing tests. 

## Pipeline
- There is a basic pipeline running static analysis on the code and executing all tests on push in to main branch.
