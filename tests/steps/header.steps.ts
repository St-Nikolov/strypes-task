import { createBdd } from 'playwright-bdd';
import { test } from '../../src/lib/fixtures/test-extension';

const { When } = createBdd(test);

When('clicks on {string} in the header', async({header} , menuItem: string) => {
    
    await header.clickOnMenuItem(menuItem);
})

When('clicks on sub menu {string} in the header', async({header} , menuPath: string) => {
    
    await header.clickOnSubMenuItem(menuPath);
})

When('clicks on the logo in the header',async ({header}) => {
    await header.clickOnTheLogo();
})

When('clicks on Search icon in the header',async ({header}) => {
    await header.clickOnSearch();
})
