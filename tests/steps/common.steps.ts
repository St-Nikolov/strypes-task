import { createBdd } from 'playwright-bdd';
import { test } from '../../src/lib/fixtures/test-extension';

const { Given, When, Then } = createBdd(test);

import { expect  } from '@playwright/test';
import { generateRandomString } from '../../src/lib/utils';

Given('random search term is generated',async ({data}) => {
  data["searchTerm"] = generateRandomString();
})

When('searches {string} by the search input field',async ( {basePage}, searchTerm: string) => {
    await basePage.searchBySearchScreen(searchTerm);
})

When('searches for the previously generated term by the search input field', async ({basePage,data}) => {
    await basePage.searchBySearchScreen(data["searchTerm"]);
})

Then('sees that page title is {string}',async ( {basePage}, expectedTitle: string) => {
    expect(await basePage.getTitle(),"Page title is correct").toBe(expectedTitle);
})

Then('sees that {string} section is available in Search results page',async ({searchResultsPage}, resultTitle: string) => {
    expect(await searchResultsPage.getResultLinkLocator(resultTitle),`${resultTitle} is available in search results`).toBeVisible();
})

Then('sees that page title is contains the previously generated term', async ({basePage, data}) => {
    expect(await basePage.getTitle(),"Page title is correct").toBe(`${data["searchTerm"]} - Strypes`);
})

Then('sees that the previously generated term is populated in the input of Search results page', async ({searchResultsPage, data}) => {
    expect(await searchResultsPage.getSearchInputLocator(),`${data["searchTerm"]} is prepopulated in search input`).toHaveValue(data["searchTerm"]);
})
  