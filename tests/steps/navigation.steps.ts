import { createBdd } from 'playwright-bdd';
import { test } from '../../src/lib/fixtures/test-extension';

const { Given } = createBdd(test);

Given("Home page is loaded",async ({homePage}) => {
    await homePage.navigate();
})

Given("Services page is loaded",async ({servicesPage}) => {
    await servicesPage.navigate();
})



