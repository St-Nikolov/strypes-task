@Header
Feature: Header links and buttons work as expected

  @ST001
  Scenario: Clickin on the logo in the header leads to the Home page
    Given Services page is loaded
    When clicks on the logo in the header
    Then sees that page title is "Strypes | End-to-end software solutions"

  @ST002
  Scenario Outline: Clicking on link in the header leads to the propper page
    Given Home page is loaded
    When clicks on "<link>" in the header
    Then sees that page title is "<title>"
    Examples:
      | link            | title                                                          |
      | About           | About Strypes \| Outsource Software Development \| Nearshoring |
      | Services        | Complete Business Software Solutions \| Services \| Strypes    |
      | Customers       | Customers - Strypes                                            |
      | Nearsurance     | Nearsurance \| Software Development Outsourcing \| Strypes     |
      | Resources       | Resources - Strypes                                            |
      | Careers         | Careers - Strypes                                              |
      | About ICT Group | About ICT Group - Strypes                                      |
      | Contact         | Contact - Strypes                                              |

  @ST003
  Scenario Outline: Clicking on sub menu link in the header leads to the propper page
    Given Home page is loaded
    When clicks on sub menu "<link>" in the header
    Then sees that page title is "<title>"
    Examples:
      | link                                                                 | title                                                           |
      | About > Our brands                                                   | Our brands - Strypes                                            |
      | About > Our promises                                                 | Our promises - Strypes                                          |
      | About > Our leadership                                               | Our leadership - Strypes                                        |
      | Services > DevOps                                                    | DevOps \| End-to-end solutions \| Strypes                       |
      | Services > DevOps > IT Infrastructure                                | IT Infrastructure management \| DevOps \| Strypes               |
      | Services > DevOps > Cybersecurity                                     | Cybersecurity with Strypes     |
      | Services > Digital transformation                                    | Digital Transformation \| Transforming Businesses \| Strypes     |
      | Services > Digital transformation > Consultancy                      | Consultancy \| Custom Software Development Consulting \|Strypes |
      | Services > Mobility and Transportation                               | Mobility & Transportation\|Smart Mobility Solutions\|Strypes  |
      | Services > Remote Diagnostics, Monitoring and Predictive Maintenance | Remote Diagnostics and Monitoring \| Predictive Maintenance     |
      | Services > SDaaS                                                     | SDaaS \| The Ultimate Software Development as a Service         |
      | Services > Smart applications                                        | Smart Applications \| Intelligent Development \| Modernization     |
      | Services > Smart applications > Application modernization            | Application Modernization \| Smart Applications \| Strypes      |
      | Services > Smart applications > Application management               | Application Management \| Smart Applications \| Strypes         |
      | Services > Smart applications > Application development              | Application Development \| Smart Applications \| Strypes        |
      | Services > Modularity Services                                       | Modularity Sevices \| Software Modularity \| Strypes            |
      | Resources > Blog                                                     | Blog - Strypes                                                  |
      | Resources > Whitepapers                                              | Whitepapers \| Powerful Software Solutions \| Strypes           |
      | Resources > Success Stories                                          | Success Stories \| Software Solutions \| Strypes                |

  @Search @ST004
  Scenario: Global search works correctly
      Given Home page is loaded
      When clicks on Search icon in the header
      And searches "Automation QA" by the search input field
      Then sees that page title is "Automation QA - Strypes"
      And sees that "Automation Quality Assurance Engineer" section is available in Search results page
  
  #This is not a real test case, but a demo for passing data between the steps.
  @Search @ST005
  Scenario: Global search works correctly with random string
      Given random search term is generated
      And Home page is loaded
      When clicks on Search icon in the header
      And searches for the previously generated term by the search input field
      Then sees that page title is contains the previously generated term
      And sees that the previously generated term is populated in the input of Search results page