import { Locator, Page } from "@playwright/test";
import { BasePage } from "./base-page"

export class SearchResultsPage extends BasePage {

    private searchResultsInputLocator;

    private resultLinkLocatorByTitle : (title:string) => Locator

    constructor (page: Page) {
        super(page);

        this.url = './s'

        this.searchResultsInputLocator = page.getByRole('searchbox', { name: 'Search', exact: true });

        this.resultLinkLocatorByTitle = (title:string) => {
                return page.locator("article .elementor-post__title").getByRole('link', {name: title})
            }
    } 

    async getResultLinkLocator(title:string): Promise<Locator>{
        return this.resultLinkLocatorByTitle(title);
    }

    async getSearchInputLocator(): Promise<Locator> {
        return this.searchResultsInputLocator;
    }
}