import { Locator, Page } from "@playwright/test";
import { BaseUIObject } from "./base-ui-object";
 
export class BasePage extends BaseUIObject{
    protected url: string;

    private searchInputLocator : Locator;
    
    constructor (page: Page) {
        super(page);
        this.searchInputLocator = page.getByRole('searchbox');
    } 

    async navigate() {
       await this.page.goto(this.url);
    }

    async getTitle() {
        return this.page.title();
    }
    
    async searchBySearchScreen(searchTerm: string) {
        await this.searchInputLocator.fill(searchTerm);
        await this.page.keyboard.press('Enter');
        await this.page.waitForLoadState("networkidle");
    }   
}