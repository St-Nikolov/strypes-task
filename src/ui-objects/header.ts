import { BaseUIObject } from "./base-ui-object";
import { Page , Locator} from "@playwright/test";

export class Header extends BaseUIObject {

    private containerLocator: Locator;
    private logoLocator: Locator;
    private searchLocator: Locator;

    private menuItemLocator: (menuItem:string) => Locator

    constructor (page: Page) {
        super(page);

        this.containerLocator = page.locator("[data-elementor-type='header']");

        this.logoLocator = this.containerLocator.getByRole("link", {name:"Strypes logo", });
        this.searchLocator = this.containerLocator.getByRole('button', { name: 'Search' });

        this.menuItemLocator = this.menuItemLocatorFunction;
    }

    async clickOnTheLogo() {
        await this.logoLocator.click();
    }
    
    async clickOnSearch() {
        await this.searchLocator.click();
    }

    async clickOnMenuItem(menuItem: string) {
        
        const currentMenuItemLocator = this.menuItemLocator(menuItem);

        await this.customHover(currentMenuItemLocator);
        await currentMenuItemLocator.click();
    }

    async clickOnSubMenuItem(menuPath: string) {
        const menuPathArray = menuPath.split('>').map((menuItem) => menuItem=menuItem.trim());
        // await this.customHover(this.menuItemLocator(menuPathArray[0]));
        for (let i = 0; i < menuPathArray.length; i++) {
            // await this.menuItemLocator(menuPathArray[i]).hover()
            await this.customHover(this.menuItemLocator(menuPathArray[i]))
        }
        await this.menuItemLocator(menuPathArray[menuPathArray.length-1]).click()

    }

    // built in hover() doesn't activate the drop down
    private async customHover(locator: Locator) {
        const boundingBox = await locator.boundingBox();    
        await this.page.mouse.move(boundingBox!.x -1 , boundingBox!.y -1 );    
        await this.page.mouse.move(boundingBox!.x + boundingBox!.width/2, boundingBox!.y + boundingBox!.height/2, {steps:20});
    }

    private menuItemLocatorFunction(menuItem : string) : Locator {
        let menuItemLocator = this.containerLocator.getByRole("link", {name:menuItem, });

        //Two items are found by text 'About', the first(0) one should be reached by the whole text - 'About ITC Group'.
        if (menuItem.toLowerCase() === "about") {
            menuItemLocator = menuItemLocator.nth(1);
        } else {
            menuItemLocator = menuItemLocator.nth(0);
        }

        return menuItemLocator;
    }
}