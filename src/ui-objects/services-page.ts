import { Page } from "@playwright/test";
import { BasePage } from "./base-page";

export class ServicesPage extends BasePage {

    constructor (page: Page) {
        super(page);

        this.url = './services/'
    } 
}