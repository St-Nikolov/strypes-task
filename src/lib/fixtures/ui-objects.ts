import { Header } from '../../ui-objects/header';
import { HomePage } from '../../ui-objects/home-page';
import { BasePage } from '../../ui-objects/base-page';
import { ServicesPage } from '../../ui-objects/services-page';
import { SearchResultsPage } from '../../ui-objects/search-results-page';

export type UIObjects = {
    basePage: BasePage;
    header: Header;
    homePage: HomePage;
    servicesPage: ServicesPage;
    searchResultsPage: SearchResultsPage;
}
