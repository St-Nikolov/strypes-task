import {test as baseTest} from 'playwright-bdd';
import { UIObjects } from './ui-objects'; 
import { Header } from '../../ui-objects/header';
import { HomePage } from '../../ui-objects/home-page';
import { BasePage } from '../../ui-objects/base-page';
import { ServicesPage } from '../../ui-objects/services-page';
import { SearchResultsPage } from '../../ui-objects/search-results-page';
import { ScenarioContext } from './scenario-context';

export const test = baseTest.extend<UIObjects>({
    basePage: async({page}, use) => {
            await use(new BasePage(page))
        },
    header: async({page}, use) => {
            await use(new Header(page));
        }, 
    homePage: async({page}, use) => {
            await use(new HomePage(page));
        },
    servicesPage: async({page}, use) => {
            await use(new ServicesPage(page));
        },
    searchResultsPage: async({page}, use) => {
            await use(new SearchResultsPage(page));
        }
}).extend<ScenarioContext>({
    // eslint-disable-next-line no-empty-pattern
    data: async({}, use) => {
        await use({});
    },
})