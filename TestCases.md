 - ##  Clickin on the logo in the header leads to the Home page
|                 info |              |     |     bdd steps |                                                                    |                    output |          |
| -------------------: | ------------ | --- | ------------: | ------------------------------------------------------------------ | ------------------------: | -------- |
|              __id:__ | ST001        |     | **Scenario:** | Clickin on the logo in the header leads to the Home page           | **Last manual execution** |          |
|        __priority:__ | MEDIUM       |     |     **Given** | Services page is loaded                                            |                 **date:** | 06.06.24 |
|     __description:__ |              |     |      **When** | clicks on the logo in the header                                   |                   **by:** | Stan     |
|        **Features:** | Home, Header |     |      **Then** | sees that page title is "Strypes \| End-to-end software solutions" |               **result:** | Pass     |
| **Number of tests:** | 1            |     |               |                                                                    |                           |          |
|    **Created date:** | 02.06.24     |     |               |                                                                    |            **Automated:** | Yes      |
|      **Created by:** | Stan         |     |               |                                                                    |                           |
|     **Reviewed by:** |              |     |               |                                                                    |
|                      |              |     |               |

---
- ### Clicking on link in the header leads to the propper page
|                 info |          |     |             bdd steps |                                                          |                    output |          |
| -------------------: | -------- | --- | --------------------: | -------------------------------------------------------- | ------------------------: | -------- |
|              __id:__ | ST002    |     | **Scenario Outline:** | Clicking on link in the header leads to the propper page | **Last manual execution** |          |
|        __priority:__ | HIGH     |     |             **Given** | Home page is loaded                                      |                 **date:** | 06.06.24 |
|     __description:__ |          |     |              **When** | clicks on "\<link\>" in the header                       |                   **by:** | Stan     |
|        **Features:** | Header   |     |              **Then** | sees that page title is "<title\>"                       |               **result:** | Pass     |
| **Number of tests:** | 8        |     |                       |                                                          |                           |          |
|    **Created date:** | 02.06.24 |     |                       |                                                          |            **Automated:** | Yes      |
|      **Created by:** | Stan     |     |                       |                                                          |                           |
|     **Reviewed by:** |          |     |                       |                                                          |
|                      |          |     |                       |

#### Examples:
| link            | title                                                          |
| --------------- | -------------------------------------------------------------- |
| About           | About Strypes \| Outsource Software Development \| Nearshoring |
| Services        | Complete Business Software Solutions \| Services \| Strypes    |
| Customers       | Customers - Strypes                                            |
| Nearsurance     | Nearsurance \| Software Development Outsourcing \| Strypes     |
| Resources       | Resources - Strypes                                            |
| Careers         | Careers - Strypes                                              |
| About ICT Group | About ICT Group - Strypes                                      |
| Contact         | Contact - Strypes                                              |

---
- ### Clicking on sub menu link in the header leads to the propper page
|                 info |          |     |             bdd steps |                                                                   |                    output |          |
| -------------------: | -------- | --- | --------------------: | ----------------------------------------------------------------- | ------------------------: | -------- |
|              __id:__ | ST003    |     | **Scenario Outline:** | Clicking on sub menu link in the header leads to the propper page | **Last manual execution** |          |
|        __priority:__ | HIGH     |     |             **Given** | Home page is loaded                                               |                 **date:** | 06.06.24 |
|     __description:__ |          |     |              **When** | clicks on sub menu "<link\>" in the header                        |                   **by:** | Stan     |
|        **Features:** | Header   |     |              **Then** | sees that page title is "<title\>"                                |               **result:** | Pass     |
| **Number of tests:** | 19       |     |                       |                                                                   |                           |          |
|    **Created date:** | 02.06.24 |     |                       |                                                                   |            **Automated:** | Yes      |
|      **Created by:** | Stan     |     |                       |                                                                   |                           |
|     **Reviewed by:** |          |     |                       |                                                                   |
|                      |          |     |                       |

#### Examples:
| link                                                                 | title                                                           |
| -------------------------------------------------------------------- | --------------------------------------------------------------- |
| About > Our brands                                                   | Our brands - Strypes                                            |
| About > Our promises                                                 | Our promises - Strypes                                          |
| About > Our leadership                                               | Our leadership - Strypes                                        |
| Services > DevOps                                                    | DevOps \| End-to-end solutions \| Strypes                       |
| Services > DevOps > IT Infrastructure                                | IT Infrastructure management \| DevOps \| Strypes               |
| Services > DevOps > Cybersecurity                                    | Cybersecurity with Strypes                                      |
| Services > Digital transformation                                    | Digital Transformation \| Transforming Businesses \| Strypes    |
| Services > Digital transformation > Consultancy                      | Consultancy \| Custom Software Development Consulting \|Strypes |
| Services > Mobility and Transportation                               | Mobility & Transportation\|Smart Mobility Solutions\|Strypes    |
| Services > Remote Diagnostics, Monitoring and Predictive Maintenance | Remote Diagnostics and Monitoring \| Predictive Maintenance     |
| Services > SDaaS                                                     | SDaaS \| The Ultimate Software Development as a Service         |
| Services > Smart applications                                        | Smart Applications \| Intelligent Development \| Modernization  |
| Services > Smart applications > Application modernization            | Application Modernization \| Smart Applications \| Strypes      |
| Services > Smart applications > Application management               | Application Management \| Smart Applications \| Strypes         |
| Services > Smart applications > Application development              | Application Development \| Smart Applications \| Strypes        |
| Services > Modularity Services                                       | Modularity Sevices \| Software Modularity \| Strypes            |
| Resources > Blog                                                     | Blog - Strypes                                                  |
| Resources > Whitepapers                                              | Whitepapers \| Powerful Software Solutions \| Strypes           |
| Resources > Success Stories                                          | Success Stories \| Software Solutions \| Strypes                |

---
- ###  Global search works correctly
|                 info |                |     |     bdd steps |                                                                                               |                    output |          |
| -------------------: | -------------- | --- | ------------: | --------------------------------------------------------------------------------------------- | ------------------------: | -------- |
|              __id:__ | ST004          |     | **Scenario:** | Global search works correctly                                                                 | **Last manual execution** |          |
|        __priority:__ | MEDIUM         |     |     **Given** | Home page is loaded                                                                           |                 **date:** | 06.06.24 |
|     __description:__ |                |     |      **When** | searches "Automation QA" by the search input field                                            |                   **by:** | Stan     |
|        **Features:** | Search, Header |     |       **And** | sees that page title is "Strypes \| End-to-end software solutions"                            |               **result:** | Pass     |
| **Number of tests:** | 1              |     |      **Then** | sees that page title is "Automation QA - Strypes"                                             |                           |          |
|    **Created date:** | 02.06.24       |     |       **And** | sees that "Automation Quality Assurance Engineer" section is available in Search results page |            **Automated:** | Yes      |
|      **Created by:** | Stan           |     |               |                                                                                               |                           |
|     **Reviewed by:** |                |     |               |                                                                                               |
|                      |                |     |               |

---
- ###  Global search works correctly with random string  
*This is not a real test case, but a demo for passing data between the steps.*

|                 info |                |     |     bdd steps |                                                                                          |                    output |          |
| -------------------: | -------------- | --- | ------------: | ---------------------------------------------------------------------------------------- | ------------------------: | -------- |
|              __id:__ | ST005          |     | **Scenario:** | Global search works correctly with random string                                         | **Last manual execution** |          |
|        __priority:__ | LOW            |     |     **Given** | random search term is generated                                                          |                 **date:** | 06.06.24 |
|     __description:__ |                |     |       **And** | Home page is loaded                                                                      |                   **by:** | Stan     |
|        **Features:** | Search, Header |     |      **When** | clicks on Search icon in the header                                                      |               **result:** | Pass     |
| **Number of tests:** | 1              |     |       **And** | searches for the previously generated term by the search input field                     |                           |          |
|    **Created date:** | 02.06.24       |     |      **Then** | sees that page title is contains the previously generated term                           |            **Automated:** | Yes      |
|      **Created by:** | Stan           |     |       **And** | sees that the previously generated term is populated in the input of Search results page |                           |
|     **Reviewed by:** |                |     |               |                                                                                          |
|                      |